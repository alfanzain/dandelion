module.exports = [
  {
    key: 'main_p2h_timesheet_digital',
    name: 'P2H & Timesheet',
    icon: 'ion-ios-analytics',
    child: [
      {
        key: 'dashboard',
        name: 'Dashboard',
        title: true,
      },
      {
        key: 'dashboard_daily_monitoring',
        name: 'Daily Monitoring',
        link: '/app/p2h-timesheet-digital/dashboard/daily-monitoring',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'report',
        name: 'Report',
        title: true,
      },
      {
        key: 'operator_experience',
        name: 'Op Exp',
        link: '/app/p2h-timesheet-digital/reports/operator-experience',
        icon: 'ion-ios-home-outline',
      },
      // {
      //   key: 'operator_experience_by_equipment',
      //   name: 'Op Exp by Equipment',
      //   link: '/app/p2h-timesheet-digital/reports/operator-experience-by-equipment',
      //   icon: 'ion-ios-home-outline',
      // },
      // {
      //   key: 'operator_experience_by_document',
      //   name: 'Op Exp By Document',
      //   link: '/app/p2h-timesheet-digital/reports/operator-experience-by-document',
      //   icon: 'ion-ios-home-outline',
      // },
      {
        key: 'timesheet',
        name: 'Timesheet',
        link: '/app/p2h-timesheet-digital/reports/timesheet',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'item_timesheet',
        name: 'Item Timesheet',
        link: '/app/p2h-timesheet-digital/reports/item-timesheet',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'p2h',
        name: 'P2H',
        link: '/app/p2h-timesheet-digital/reports/p2h',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'item_p2h',
        name: 'Item P2H',
        link: '/app/p2h-timesheet-digital/reports/item-p2h',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'temuan_p2h',
        name: 'Temuan P2H',
        link: '/app/p2h-timesheet-digital/reports/temuan-p2h',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'counter_reading',
        name: 'Counter Reading',
        link: '/app/p2h-timesheet-digital/reports/counter-reading',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'daily_monitoring',
        name: 'Daily Monitoring',
        link: '/app/p2h-timesheet-digital/reports/daily-monitoring',
        icon: 'ion-ios-home-outline',
      },
    ]
  },
  {
    key: 'pages',
    name: 'Pages',
    icon: 'ion-ios-paper-outline',
    child: [
      {
        key: 'other_page',
        name: 'Welcome Page',
        title: true,
      },
      {
        key: 'blank',
        name: 'Blank Page',
        link: '/app',
        icon: 'ion-ios-document-outline',
      },
      {
        key: 'main_page',
        name: 'Sample Page',
        title: true,
      },
      {
        key: 'dashboard',
        name: 'Dashboard',
        link: '/app/pages/dashboard',
        icon: 'ion-ios-home-outline',
      },
      {
        key: 'form',
        name: 'Form',
        link: '/app/pages/form',
        icon: 'ion-ios-list-box-outline',
      },
      {
        key: 'table',
        name: 'Table',
        link: '/app/pages/table',
        icon: 'ion-ios-grid-outline',
      },
      {
        key: 'maintenance',
        name: 'Maintenance',
        link: '/maintenance',
        icon: 'ion-ios-build-outline'
      },
      {
        key: 'coming_soon',
        name: 'Coming Soon',
        link: '/coming-soon',
        icon: 'ion-ios-bonfire-outline'
      },
    ]
  },
  {
    key: 'auth',
    name: 'Auth Page',
    icon: 'ion-ios-contact-outline',
    child: [
      {
        key: 'auth_page',
        name: 'User Authentication',
        title: true,
      },
      {
        key: 'login',
        name: 'Login',
        link: '/login',
        icon: 'ion-ios-person-outline'
      },
      {
        key: 'register',
        name: 'Register',
        link: '/register',
        icon: 'ion-ios-key-outline'
      },
      {
        key: 'reset',
        name: 'Reset Password',
        link: '/reset-password',
        icon: 'ion-ios-undo-outline'
      },
    ]
  },
  {
    key: 'errors',
    name: 'Errors',
    icon: 'ion-ios-paw-outline',
    child: [
      {
        key: 'errors_page',
        name: 'Errors Pages',
        title: true,
      },
      {
        key: 'not_found_page',
        name: 'Not Found Page',
        link: '/app/pages/not-found',
        icon: 'ion-ios-warning-outline'
      },
      {
        key: 'error_page',
        name: 'Error Page',
        link: '/app/pages/error',
        icon: 'ion-ios-warning-outline'
      },
    ]
  },
  {
    key: 'menu_levels',
    name: 'Menu Levels',
    multilevel: true,
    icon: 'ion-ios-menu-outline',
    child: [
      {
        key: 'level_1',
        name: 'Level 1',
        link: '/#'
      },
      {
        key: 'level_2',
        keyParent: 'menu_levels',
        name: 'Level 2',
        child: [
          {
            key: 'sub_menu_1',
            name: 'Sub Menu 1',
            link: '/#'
          },
          {
            key: 'sub_menu_2',
            name: 'Sub Menu 2',
            link: '/#'
          },
        ]
      },
    ]
  },
  {
    key: 'no_child',
    name: 'One Level Menu',
    icon: 'ion-ios-document-outline',
    linkParent: '/app/blank-page',
  }
];
