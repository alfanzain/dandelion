import { teal } from '@material-ui/core/colors';

export const sortArrayOfString = (arr) => {
  return arr.sort((a, b) => {
    const nameA = a.toUpperCase(); // ignore upper and lowercase
    const nameB = b.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  });
}

export const sortArrayOfStringOfObject = (arr, key) => {
  return arr.sort((a, b) => {
    const nameA = a[key].toUpperCase(); // ignore upper and lowercase
    const nameB = b[key].toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  });
}

export const mapToArray = (arr, key) => {
  return arr.map(item => item[key]);
}

export const mapToArrayOfObject = (arr, codeKey, valueKey) => {
  return arr.map(item => ({
      text: `${item[codeKey]} - ${item[valueKey]}`,
      value: item[codeKey],
    })
  ).map(JSON.stringify);
}

export const makeSetOfArray = (arr, key) => [...new Set(mapToArray(arr, key))]

export const makeSetOfArrayOfObject = (arr, codeKey, valueKey) => [...new Set(mapToArrayOfObject(arr, codeKey, valueKey))].map(JSON.parse)

export const generateSimpleTableData = (data) => {
  let dataTable = [];

  data.forEach(items => {
    let item = [];

    for (const property in items) {
      item.push(items[property]);
    }

    dataTable.push(item);
  });

  return dataTable;
}

export const generateSimpleChartData = (data, labelKey, dataKey) => ({
  labels: data.map(item => item[labelKey]),
  datasets: [
    {
      backgroundColor: teal[400],
      hoverBackgroundColor: teal[100],
      data: data.map(item => item[dataKey])
    }
  ]
});

export const generateSumGroupedLabelChartData = (data, groupKey, sumKey) => {
  let groupLabel = [...new Set(data.map(item => item[groupKey]))];
  let groupSum = [];

  groupLabel.forEach(label => {
    let total = 0;
    
    data.forEach(item => {
      if (item[groupKey] === label) total += item[sumKey]
    });

    groupSum.push(total);
  });

  return {
    labels: groupLabel,
    datasets: [
      {
        backgroundColor: teal[400],
        hoverBackgroundColor: teal[100],
        data: groupSum
      }
    ]
  };
};

export const generateCountGroupedLabelChartData = (data, groupKey) => {
  let groupLabel = [...new Set(data.map(item => item[groupKey]))];
  let groupSum = [];

  groupLabel.forEach(label => {
    let total = 0;
    
    data.forEach(item => {
      if (item[groupKey] === label) total += 1
    });

    groupSum.push(total);
  });

  return {
    labels: groupLabel,
    datasets: [
      {
        backgroundColor: groupLabel.map(() => teal[400]),
        hoverBackgroundColor: teal[100],
        borderColor: teal[800],
        borderWidth: groupLabel.map(() => 0),
        data: groupSum,
      }
    ]
  };
};