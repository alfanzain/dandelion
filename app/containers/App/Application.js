import React, { useContext } from 'react';
import { PropTypes } from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import Dashboard from '../Templates/Dashboard';
import { ThemeContext } from './ThemeWrapper';
import {
  Parent,
  DashboardPage,
  BlankPage,
  Form,
  Table,
  Error,
  NotFound,
  P2hAndTimesheetReportDailyMonitoring,
  P2hAndTimesheetReportOperatorExperience,
  P2hAndTimesheetReportOperatorExperienceByEquipment,
  P2hAndTimesheetReportOperatorExperienceByDocument,
  P2hAndTimesheetReportTimesheet,
  P2hAndTimesheetReportItemTimesheet,
  P2hAndTimesheetReportP2h,
  P2hAndTimesheetReportItemP2h,
  P2hAndTimesheetReportTemuanP2h,
  P2hAndTimesheetReportCounterReading,
  P2hAndTimesheetDashboardDailyMonitoring
} from '../pageListAsync';

function Application(props) {
  const { history } = props;
  const changeMode = useContext(ThemeContext);

  return (
    <Dashboard history={history} changeMode={changeMode}>
      <Switch>
        <Route exact path="/app" component={BlankPage} />
        <Route exact path="/app/blank-page" component={BlankPage} />
        <Route path="/app/pages/dashboard" component={DashboardPage} />
        <Route path="/app/pages/form" component={Form} />
        <Route path="/app/pages/table" component={Table} />
        <Route path="/app/pages/not-found" component={NotFound} />
        <Route path="/app/pages/error" component={Error} />
        <Route exact path="/app/pages" component={Parent} />
        <Route path="/app/p2h-timesheet-digital/reports/operator-experience" component={P2hAndTimesheetReportOperatorExperience} />
        <Route path="/app/p2h-timesheet-digital/reports/operator-experience-by-equipment" component={P2hAndTimesheetReportOperatorExperienceByEquipment} />
        <Route path="/app/p2h-timesheet-digital/reports/operator-experience-by-document" component={P2hAndTimesheetReportOperatorExperienceByDocument} />
        <Route path="/app/p2h-timesheet-digital/reports/timesheet" component={P2hAndTimesheetReportTimesheet} />
        <Route path="/app/p2h-timesheet-digital/reports/item-timesheet" component={P2hAndTimesheetReportItemTimesheet} />
        <Route path="/app/p2h-timesheet-digital/reports/p2h" component={P2hAndTimesheetReportP2h} />
        <Route path="/app/p2h-timesheet-digital/reports/item-p2h" component={P2hAndTimesheetReportItemP2h} />
        <Route path="/app/p2h-timesheet-digital/reports/temuan-p2h" component={P2hAndTimesheetReportTemuanP2h} />
        <Route path="/app/p2h-timesheet-digital/reports/counter-reading" component={P2hAndTimesheetReportCounterReading} />
        <Route path="/app/p2h-timesheet-digital/reports/daily-monitoring" component={P2hAndTimesheetReportDailyMonitoring} />
        <Route path="/app/p2h-timesheet-digital/dashboard/daily-monitoring" component={P2hAndTimesheetDashboardDailyMonitoring} />
        <Route component={NotFound} />
      </Switch>
    </Dashboard>
  );
}

Application.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Application;
