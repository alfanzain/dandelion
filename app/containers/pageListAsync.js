/* eslint-disable */

import React from 'react';
import Loading from 'dan-components/Loading';
import loadable from '../utils/loadable';

export const BlankPage = loadable(() =>
  import('./Pages/BlankPage'), {
    fallback: <Loading />,
  });
export const DashboardPage = loadable(() =>
  import('./Pages/Dashboard'), {
    fallback: <Loading />,
  });
export const Form = loadable(() =>
  import('./Pages/Forms/ReduxForm'), {
    fallback: <Loading />,
  });
export const Table = loadable(() =>
  import('./Pages/Table/BasicTable'), {
    fallback: <Loading />,
  });
export const Login = loadable(() =>
  import('./Pages/Users/Login'), {
    fallback: <Loading />,
  });
export const LoginDedicated = loadable(() =>
  import('./Pages/Standalone/LoginDedicated'), {
    fallback: <Loading />,
  });
export const Register = loadable(() =>
  import('./Pages/Users/Register'), {
    fallback: <Loading />,
  });
export const ResetPassword = loadable(() =>
  import('./Pages/Users/ResetPassword'), {
    fallback: <Loading />,
  });
export const NotFound = loadable(() =>
  import('./NotFound/NotFound'), {
  fallback: <Loading />,
});
export const NotFoundDedicated = loadable(() =>
  import('./Pages/Standalone/NotFoundDedicated'), {
    fallback: <Loading />,
  });
export const Error = loadable(() =>
  import('./Pages/Error'), {
    fallback: <Loading />,
  });
export const Maintenance = loadable(() =>
  import('./Pages/Maintenance'), {
    fallback: <Loading />,
  });
export const ComingSoon = loadable(() =>
  import('./Pages/ComingSoon'), {
    fallback: <Loading />,
  });
export const Parent = loadable(() =>
  import('./Parent'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportOperatorExperience = loadable(() =>
  import('./P2hAndTimesheet/Reports/OperatorExperience'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportOperatorExperienceByEquipment = loadable(() =>
  import('./P2hAndTimesheet/Reports/OperatorExperienceByEquipment'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportOperatorExperienceByDocument = loadable(() =>
  import('./P2hAndTimesheet/Reports/OperatorExperienceByDocument'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportTimesheet = loadable(() =>
  import('./P2hAndTimesheet/Reports/Timesheet'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportItemTimesheet = loadable(() =>
  import('./P2hAndTimesheet/Reports/ItemTimesheet'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportP2h = loadable(() =>
  import('./P2hAndTimesheet/Reports/P2h'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportItemP2h = loadable(() =>
  import('./P2hAndTimesheet/Reports/ItemP2h'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportTemuanP2h = loadable(() =>
  import('./P2hAndTimesheet/Reports/TemuanP2h'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportCounterReading = loadable(() =>
  import('./P2hAndTimesheet/Reports/CounterReading'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetReportDailyMonitoring = loadable(() =>
  import('./P2hAndTimesheet/Reports/DailyMonitoring'), {
    fallback: <Loading />,
  });
export const P2hAndTimesheetDashboardDailyMonitoring = loadable(() =>
  import('./P2hAndTimesheet/Dashboard/DailyMonitoring'), {
    fallback: <Loading />,
  });