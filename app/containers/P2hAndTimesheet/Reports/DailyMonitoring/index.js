import React, { useState } from 'react';
import { Helmet } from 'react-helmet';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import Filter from './Filter';
import BarDataChart from './BarDataChart';
import MainTable from './MainTable';

import { createDummyData } from './dummy-data.js';

const REPORT_MODE_CHART_AND_TABLE_CODE = 1; 
const REPORT_MODE_TABLE_CODE = 2;
const REPORT_MODE_CHART_CODE = 3;

const DOCUMENT_STATUS_CREATED = 'created';
const DOCUMENT_STATUS_NOT_YET = 'not_yet';

const setDataChart = (data) => {
    // {
    //   name: 'Page A',
    //   uv: 4000,
    //   pv: 2400,
    //   amt: 2400
    // },

  let p2hCreatedCount = 0;
  let p2hNotYetCount = 0;
  let timesheetCreatedCount = 0;
  let timesheetNotYetCount = 0;
  
  data.forEach((item) => {
    item.p2h_status === DOCUMENT_STATUS_CREATED 
      ? p2hCreatedCount++
      : p2hNotYetCount++;

    item.timesheet_status === DOCUMENT_STATUS_CREATED 
      ? timesheetCreatedCount++
      : timesheetNotYetCount++;
  });

  return [{
    name: 'P2H',
    created: p2hCreatedCount,
    not_yet: p2hNotYetCount,
    total: p2hCreatedCount + p2hNotYetCount
  }, {
    name: 'Timesheet',
    created: timesheetCreatedCount,
    not_yet: timesheetNotYetCount,
    total: timesheetCreatedCount + timesheetNotYetCount
  }];
};

const setDataTable = (data) => {
  let dataTable = [];

  data.forEach(items => {
    let item = [];

    for (const property in items) {
      item.push(items[property]);
    }

    dataTable.push(item);
  });

  return dataTable;
}

function DailyMonitoring() {
  const title = 'Daily Monitoring';
  const description = '';
  const [dataFilter, setDataFilter] = useState({
    company: '',
    project: '',
    equipmentCode: '',
    createdBy: '',
    reportDate: '',
  });
  const [reportMode, setReportMode] = useState(REPORT_MODE_CHART_AND_TABLE_CODE);
  const data = createDummyData();
  const dataChart = setDataChart(data);
  const dataTable = setDataTable(data);

  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Paper style={{ paddingTop: 32, paddingBottom: 32 }}>
        <Grid
          container
          spacing={0}
        >
          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={8}>
              <Filter 
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item xs={8}>
              { 
                [REPORT_MODE_CHART_CODE, REPORT_MODE_CHART_AND_TABLE_CODE].includes(reportMode) 
                  ? <BarDataChart 
                      data={ dataChart }
                    /> 
                  : null 
              }
            </Grid>
            <Grid 
              item 
              xs={2}
            >
              <Paper elevation={0}>
                <Grid
                  container
                  spacing={2}
                  direction="column"
                  justifyContent="flex-start"
                  alignItems="flex-end"
                >
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_CHART_AND_TABLE_CODE)}
                      color={
                        [REPORT_MODE_CHART_AND_TABLE_CODE].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Chart & Table
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_CHART_CODE)}
                      color={
                        [REPORT_MODE_CHART_CODE].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Chart Only
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_TABLE_CODE)}
                      color={
                        [REPORT_MODE_TABLE_CODE].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Table Only
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
          >
            <Grid item xs={12}>
              { 
                [REPORT_MODE_TABLE_CODE, REPORT_MODE_CHART_AND_TABLE_CODE].includes(reportMode) 
                  ? <MainTable 
                      data={ dataTable }
                    /> 
                  : null 
              }
            </Grid>
          </Grid>
        </Grid> 
      </Paper>
    </div>
  );
}

export default DailyMonitoring;
