const persons = [
  {
    code: "A0101",
    name: "Burhan",
    email: "burhan@gmail.com",
  },
  {
    code: "A0102",
    name: "Asep",
    email: "asep@gmail.com",
  },
  {
    code: "A0103",
    name: "Udin",
    email: "udin@gmail.com",
  },
  {
    code: "A0104",
    name: "Septian",
    email: "septian@gmail.com",
  },
  {
    code: "A0105",
    name: "Rudi",
    email: "rudi@gmail.com",
  },
  {
    code: "A0106",
    name: "Gatot",
    email: "gatot@gmail.com",
  },
  {
    code: "A0107",
    name: "Ifan",
    email: "ifan@gmail.com",
  },
  {
    code: "A0108",
    name: "Doso",
    email: "doso@gmail.com",
  },
  {
    code: "A0109",
    name: "Timbul",
    email: "timbul@gmail.com",
  },
  {
    code: "A0110",
    name: "Sobur Ah",
    email: "soburah@gmail.com",
  },
];
const equipments = [
  {
    code: "EX001",
    number: "10000001",
    description: "Hydraulic Excavator",
    model_no: 'PC2-8MO',
  },
  {
    code: "EX002",
    number: "10000002",
    description: "Min Loader",
    model_no: 'S570',
  },
  {
    code: "EX003",
    number: "10000003",
    description: "Compactor",
    model_no: 'BW211D-40',
  },
  {
    code: "EX004",
    number: "10000004",
    description: "Hydraulic Drilling Rig",
    model_no: 'PC2-DRL40',
  },
]
const plantOrigins = ["W001"];
const companyCodes = ["ACST"];
const positions = ["Heavy Equipment Operator"];
const phoneNumbers = ["123"];
const projects = [
  {
    code: "B001",
    name: "Thamrin 9",
  },
  {
    code: "B007",
    name: "Apartment Cleon",
  },
];
const documentStatuses = [
  'created',
  'not_yet'
]

const numberValue = (max, times) => {
  return Math.floor(Math.random() * max) * times;
};

const COUNT_DATA = 50;

export function createDummyData() {
  let data = [];

  for (let i = 0; i < COUNT_DATA; i++) {
    const person = persons[numberValue(persons.length, 1)];
    const equipment = equipments[numberValue(equipments.length, 1)];

    data.push({
      nrp: person.code,
      full_name: person.name,
      equipment_number: equipment.number,
      equipment_description: equipment.description,
      equipment_code: equipment.equipment_code,
      p2h_number: `P2H000000${String(numberValue(99, 1) + 1).padStart(2, '0')}`,
      p2h_status: documentStatuses[numberValue(documentStatuses.length, 1)],
      timesheet_number: `TS000000${String(numberValue(99, 1) + 1).padStart(2, '0')}`,
      timesheet_status: documentStatuses[numberValue(documentStatuses.length, 1)],
    });
  }

  return data;
}
