import React from "react";
import PropTypes from "prop-types";
import { createTheme, withStyles } from "@material-ui/core/styles";
import ThemePallete from "dan-api/palette/themePalette";
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    CartesianAxis,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from "recharts";
import styles from "./fluidChart-jss";
import { teal } from '@material-ui/core/colors';

const theme = createTheme(ThemePallete.magentaTheme);
const color = {
    primary: theme.palette.primary.main,
    primaryDark: theme.palette.primary.dark,
    secondary: theme.palette.secondary.main,
    secondaryDark: theme.palette.secondary.dark,
};

function BarDataChart(props) {
    const { classes, data } = props;

    return (
        <div className={classes.chartFluid}>
            <ResponsiveContainer
                className={classes.rechartsWrapper}
                width={800}
                height="100%"
            >
                <BarChart
                    width={800}
                    height={450}
                    data={data}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <defs>
                        <linearGradient
                            id="colorCreated"
                            x1="0"
                            y1="0"
                            x2="0"
                            y2="1"
                        >
                            <stop
                                offset="5%"
                                stopColor={teal[500]}
                                stopOpacity={0.8}
                            />
                            <stop
                                offset="95%"
                                stopColor={teal[700]}
                                stopOpacity={1}
                            />
                        </linearGradient>
                        <linearGradient
                            id="colorNotYet"
                            x1="0"
                            y1="0"
                            x2="0"
                            y2="1"
                        >
                            <stop
                                offset="5%"
                                stopColor={color.secondary}
                                stopOpacity={0.8}
                            />
                            <stop
                                offset="95%"
                                stopColor={color.secondaryDark}
                                stopOpacity={1}
                            />
                        </linearGradient>
                        <linearGradient
                            id="colorTotal"
                            x1="0"
                            y1="0"
                            x2="0"
                            y2="1"
                        >
                            <stop
                                offset="5%"
                                stopColor={color.primary}
                                stopOpacity={0.8}
                            />
                            <stop
                                offset="95%"
                                stopColor={color.primaryDark}
                                stopOpacity={1}
                            />
                        </linearGradient>
                    </defs>
                    <XAxis dataKey="name" tickLine={false} />
                    <YAxis
                        axisLine={false}
                        tickSize={3}
                        tickLine={false}
                        tick={{ stroke: "none" }}
                    />
                    <CartesianGrid vertical={false} strokeDasharray="3 3" />
                    <CartesianAxis vertical={false} />
                    <Tooltip />
                    <Legend />
                    <Bar name="Created" dataKey="created" fillOpacity="1" fill="url(#colorCreated)" />
                    <Bar name="Not Yet" dataKey="not_yet" fillOpacity="0.8" fill="url(#colorNotYet)" />
                    <Bar name="Total" dataKey="total" fillOpacity="0.8" fill="url(#colorTotal)" />
                </BarChart>
            </ResponsiveContainer>
        </div>
    );
}

BarDataChart.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BarDataChart);
