import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MUIDataTable from 'mui-datatables';
import Box from '@material-ui/core/Box';

const activeCircleStyles = { bgcolor: '#23ad38', width: 20, height: 20, display: 'block' };
const nonActiveCircleStyles = { bgcolor: '#e70c38', width: 20, height: 20, display: 'block' };
const shapeCircleStyles = { borderRadius: '50%' };
const activeCircle = (
  <Box component="span" sx={{ ...activeCircleStyles, ...shapeCircleStyles }} />
);
const nonActiveCircle = (
  <Box component="span" sx={{ ...nonActiveCircleStyles, ...shapeCircleStyles }} />
);

const styles = theme => ({
  table: {
    '& > div': {
      // overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          // overflow: 'hidden',
          // textOverflow: 'ellipsis'
        }
      }
    }
  }
});

function MainTable(props) {
  const columns = [
    {
      name: "NRP",
      options: {
        filter: true,
      },
    },
    {
      name: "Operator Name",
      options: {
        filter: true,
      },
    },
    {
      name: "Equipment Number",
      options: {
        filter: true,
      },
    },
    {
      name: "Equipment Description",
      options: {
        filter: false,
      },
    },
    {
      name: "Equipment Code",
      options: {
        filter: true,
      },
    },
    {
      name: "P2H Number",
      options: {
        filter: true,
      },
    },
    {
      name: "P2H Status",
      options: {
        filter: true,
        customBodyRender: (value) => {
          if (value === 'created') {
            return (activeCircle);
          }
          if (value === 'not_yet') {
            return (nonActiveCircle);
          }
          return (<></>);
        }
      },
    },
    {
      name: "Timesheet Number",
      options: {
        filter: true,
      },
    },
    {
      name: "Timesheet Status",
      options: {
        filter: true,
        customBodyRender: (value) => {
          if (value === 'created') {
            return (activeCircle);
          }
          if (value === 'not_yet') {
            return (nonActiveCircle);
          }
          return (<></>);
        }
      },
    },
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    // print: true,
    rowsPerPage: 10,
    page: 0,
    selectableRows: false
  };

  const { classes, data } = props;

  return (
    <Fragment>
      <div className={classes.table}>
        <MUIDataTable
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    </Fragment>
  );
}

MainTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainTable);
