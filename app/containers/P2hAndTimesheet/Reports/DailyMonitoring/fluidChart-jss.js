const styles = {
  chartFluid: {
    width: '100%',
    minWidth: 500,
    height: 450
  },

  rechartsWrapper: {
    margin: '0 auto',
    marginTop: '64px',
  }
};

export default styles;
