import React from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import CreatedDateFilter from "./Filters/CreatedDateFilter";
import DocumentDateFilter from "./Filters/DocumentDateFilter";
import NrpFilter from "./Filters/NrpFilter";
import DocumentNumberFilter from "./Filters/DocumentNumberFilter";

function Filter(props) {
  const { dataFilter, handleChange } = props;

  return (
    <div style={{ padding: 16, width: "100%" }}>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Filter
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={0}>
            <Grid item xs={12}>
              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-company-code">
                  Company Code
                </InputLabel>
                <Select
                  value={dataFilter.companyCode}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-company-code",
                    id: "filter-company-code",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-project">Project</InputLabel>
                <Select
                  value={dataFilter.project}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-project",
                    id: "filter-project",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-equipment-code">
                  Equipment Code
                </InputLabel>
                <Select
                  value={dataFilter.equipmentCode}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-equipment-code",
                    id: "filter-equipment-code",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-approval-1">Approval 1</InputLabel>
                <Select
                  value={dataFilter.approval1}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-approval-1",
                    id: "filter-approval-1",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-approval-2">Approval 2</InputLabel>
                <Select
                  value={dataFilter.approval2}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-approval-2",
                    id: "filter-approval-2",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-approval-3">Approval 3</InputLabel>
                <Select
                  value={dataFilter.approval3}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-approval-3",
                    id: "filter-approval-3",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <CreatedDateFilter
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>

            <Grid item xs={12}>
              <DocumentDateFilter
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>

            <Grid item xs={12}>
              <NrpFilter dataFilter={dataFilter} handleChange={handleChange} />
            </Grid>

            <Grid item xs={12}>
              <DocumentNumberFilter
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

export default Filter;
