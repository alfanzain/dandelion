import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import { lighten, darken } from "@material-ui/core/styles";

const styles = (theme) => ({
    table: {
        "& > div": {
            // overflow: 'auto'
        },
        "& table": {
            "& tbody tr:hover": {
                background:
                    theme.palette.type === "dark"
                        ? darken(theme.palette.primary.light, 0.8)
                        : lighten(theme.palette.primary.light, 0.5),
            },
            "& tr": {
                height: 24,
                "& td, th": {
                    padding: "4px 10px",
                    fontSize: 12,
                },
            },
            "& td": {
                wordBreak: "keep-all",
            },
            [theme.breakpoints.down("md")]: {
                "& td": {
                    height: 60,
                    // overflow: 'hidden',
                    // textOverflow: 'ellipsis'
                },
            },
        },
    },
});

function MainTable(props) {
    const columns = [
        {
            name: "Document Date",
            options: {
                filter: true,
            },
        },
        {
            name: "Project Code",
            options: {
                filter: true,
            },
        },
        {
            name: "Project Name",
            options: {
                filter: true,
            },
        },
        {
            name: "Document No.",
            options: {
                filter: true,
            },
        },
        {
            name: "Equipment Type",
            options: {
                filter: true,
            },
        },
        {
            name: "Equipment Desc.",
            options: {
                filter: true,
            },
        },
        {
            name: "Equipment Code",
            options: {
                filter: true,
            },
        },
        {
            name: "NRP",
            options: {
                filter: true,
            },
        },
        {
            name: "Full Name",
            options: {
                filter: true,
            },
        },
        {
            name: "Approval 1",
            options: {
                filter: true,
            },
        },
        {
            name: "Status 1",
            options: {
                filter: true,
            },
        },
        {
            name: "Date Approval 1",
            options: {
                filter: true,
            },
        },
        {
            name: "Time Approval 1",
            options: {
                filter: true,
            },
        },
        {
            name: "Approval 2",
            options: {
                filter: true,
            },
        },
        {
            name: "Status 2",
            options: {
                filter: true,
            },
        },
        {
            name: "Date Approval 2",
            options: {
                filter: true,
            },
        },
        {
            name: "Time Approval 2",
            options: {
                filter: true,
            },
        },
        {
            name: "Approval 3",
            options: {
                filter: true,
            },
        },
        {
            name: "Status 3",
            options: {
                filter: true,
            },
        },
        {
            name: "Date Approval 3",
            options: {
                filter: true,
            },
        },
        {
            name: "Time Approval 3",
            options: {
                filter: true,
            },
        },
        {
            name: "Shift",
            options: {
                filter: true,
            },
        },
        {
            name: "HM",
            options: {
                filter: true,
            },
        },
        {
            name: "Temuan",
            options: {
                filter: true,
            },
        },
        {
            name: "Action",
            options: {
                filter: true,
            },
        },
        {
            name: "Status",
            options: {
                filter: true,
            },
        },
    ];

    const options = {
        filterType: "dropdown",
        responsive: "vertical",
        rowsPerPage: 10,
        page: 0,
        selectableRows: false,
    };

    const { classes, data } = props;

    return (
        <Fragment>
            <div className={classes.table}>
                <MUIDataTable data={data} columns={columns} options={options} />
            </div>
        </Fragment>
    );
}

MainTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainTable);
