import React, { useState } from 'react';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import Type from 'dan-styles/Typography.scss';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import idLocale from 'date-fns/locale/id';
import enLocale from 'date-fns/locale/en-US';

const localeMap = {
  en: enLocale,
  id: idLocale,
};

const styles = theme => ({
  demo: {
    height: 240,
  },
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  }
});

export default function CreatedDateFilter(props) {
  const { dataFilter, handleChange } = props;
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentLocale, setCurrentLocale] = useState('id');
  const locale = localeMap[currentLocale];
  
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const handleMenuOpen = (event) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const selectLocale = (selectedLocale) => {
    setCurrentLocale(selectedLocale);
    setAnchorEl(null);
  };

  return (
    <Grid
      container
      spacing={0}
      alignItems='flex-end'
      style={{ margin: 8 }}
    >
      <Grid item xs={2}>
        <MuiPickersUtilsProvider 
          style={{ margin: 8 }}
          utils={MomentUtils}
        >
          <DatePicker
            label="Created Date"
            value={selectedDate}
            onChange={handleDateChange}
            animateYearScrolling={false}
          />
        </MuiPickersUtilsProvider>
      </Grid>
      <Grid item xs={1}>
        <Typography variant="subtitle1" className={Type.textCenter} gutterBottom>
          <span className={Type.bold}>To</span>
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <MuiPickersUtilsProvider 
          utils={MomentUtils}
        >
          <DatePicker
            value={selectedDate}
            onChange={handleDateChange}
            animateYearScrolling={false}
          />
        </MuiPickersUtilsProvider>
      </Grid>
    </Grid>
  )
}