import React, { useState } from 'react';
import { Helmet } from 'react-helmet';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import Filter from './Filter';
import MainTable from './MainTable';
import PieDataChart from './PieDataChart';
import BarDataChart from './BarDataChart';

let data = [];

function createData () {
  let statuses = [
    'SAVE',
    'SUBP',
    'SUBT',
    'PAPP',
    'APPR',
    'CLSD',
  ];

  for (let i = 1; i < 50; i++) {
    data.push({
      document_date: '-',
      project_code: '-',
      project_name: '-',
      document_no: '-',
      equipment_type: '-',
      equipment_desc: '-',
      equipment_code: '-',
      nrp: '-',
      full_name: '-',
      approval_1: '-',
      status_1: '-',
      date_approval_1: '-',
      time_approval_1: '-',
      approval_2: '-',
      status_2: '-',
      date_approval_2: '-',
      time_approval_2: '-',
      approval_3: '-',
      status_3: '-',
      date_approval_3: '-',
      time_approval_3: '-',
      shift: '-',
      hm: '-',
      temuan: '-',
      action: '-',
      status: statuses[
        Math.floor(Math.random() * statuses.length)
      ],
    });
  }
};

createData();

let groupStatus = data.map(item => item.status);
let groupCount = [];

groupStatus = [...new Set(groupStatus)];
groupStatus.forEach(status => {
  let total = 0;
  
  data.forEach(item => {
    if (item.status === status) total += 1
  });

  groupCount.push(total);
});

const dataChart = {
  labels: groupStatus.map(v => v),
  datasets: [
    {
      data: groupCount.map(v => v),
    },
  ]
};

let dataPieChart;

dataPieChart = groupStatus.map((v, i) => {
  return {
    name: v,
    value: groupCount[i],
  };
});

const REPORT_MODE_CHART_AND_TABLE_CODE = 1; 
const REPORT_MODE_TABLE_CODE = 2;
const REPORT_MODE_CHART_CODE = 3; 

function P2h() {
  const title = 'P2H';
  const description = '';
  const [dataFilter, setDataFilter] = useState({
    company: '',
    project: '',
    equipmentCode: '',
    createdDate: '',
    documentDate: '',
    nrp: '',
    documentNumber: '',
    approval1: '',
    approval2: '',
    approval3: '',
  });
  const [reportMode, setReportMode] = useState(REPORT_MODE_CHART_AND_TABLE_CODE);

  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  let dataTable = [];

  data.forEach(items => {
    let item = [];

    for (const property in items) {
      item.push(items[property]);
    }

    dataTable.push(item);
  });

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Paper style={{ paddingTop: 32, paddingBottom: 32 }}>
        <Grid
          container
          spacing={0}
        >
          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={8}>
              <Filter 
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item xs={4}>
              { 
                [REPORT_MODE_CHART_CODE, REPORT_MODE_CHART_AND_TABLE_CODE].includes(reportMode) 
                  ? <PieDataChart 
                      data={dataPieChart}
                    /> 
                  : null 
              }
            </Grid>
            <Grid item xs={4}>
              { 
                [
                  REPORT_MODE_CHART_CODE, 
                  REPORT_MODE_CHART_AND_TABLE_CODE
                ].includes(reportMode) 
                  ? <BarDataChart 
                      data={dataChart}
                    /> 
                  : null 
              }
            </Grid>
            <Grid 
              item 
              xs={2}
            >
              <Paper elevation={0}>
                <Grid
                  container
                  spacing={2}
                  direction="column"
                  justifyContent="flex-start"
                  alignItems="flex-end"
                >
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_CHART_AND_TABLE_CODE)}
                      color={
                        [REPORT_MODE_CHART_AND_TABLE_CODE].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Chart & Table
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_CHART_CODE)}
                      color={
                        [REPORT_MODE_CHART_CODE].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Chart Only
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_TABLE_CODE)}
                      color={
                        [REPORT_MODE_TABLE_CODE].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Table Only
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
          
          <Grid
            container
            spacing={0}
          >
            <Grid item xs={12}>
              <MainTable 
                data={dataTable}
              />
            </Grid>
          </Grid>
        </Grid> 
      </Paper>
    </div>
  );
}

export default P2h;
