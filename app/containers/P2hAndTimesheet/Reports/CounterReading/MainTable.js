import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import { lighten, darken } from "@material-ui/core/styles";

const styles = (theme) => ({
    table: {
        "& > div": {
            // overflow: 'auto'
        },
        "& table": {
            "& tbody tr:hover": {
                background:
                    theme.palette.type === "dark"
                        ? darken(theme.palette.primary.light, 0.8)
                        : lighten(theme.palette.primary.light, 0.5),
            },
            "& tr": {
                height: 24,
                "& td, th": {
                    padding: "4px 10px",
                    fontSize: 12,
                },
            },
            "& td": {
                wordBreak: "keep-all",
            },
            [theme.breakpoints.down("md")]: {
                "& td": {
                    height: 60,
                    // overflow: 'hidden',
                    // textOverflow: 'ellipsis'
                },
            },
        },
    },
});

const columnNames = [
    'Measurement Doc.',
    'Measuring Point',
    'Document Number',
    'Equipment Number',
    'Equipment Desc.',
    'Equipment Code',
    'Counter Reading',
    'Unit',
    'NRP',
    'Created By',
    'Created Date',
    'Created Time',
];

function createColumn() {
    return columnNames.map(column => {
        return {
            name: column,
            options: {
                filter: true,
            },
        }
    });
}

function MainTable(props) {
    const columns = createColumn();

    const options = {
        filterType: "dropdown",
        responsive: "vertical",
        rowsPerPage: 10,
        page: 0,
        selectableRows: false,
    };

    const { classes, data } = props;

    return (
        <Fragment>
            <div className={classes.table}>
                <MUIDataTable data={data} columns={columns} options={options} />
            </div>
        </Fragment>
    );
}

MainTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainTable);
