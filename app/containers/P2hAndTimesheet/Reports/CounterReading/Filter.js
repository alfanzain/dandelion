import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import FormControl from '@material-ui/core/FormControl';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import NrpFilter from './Filters/NrpFilter'
import EquipmentNumberFilter from './Filters/EquipmentNumberFilter'
import MeasuringPointFilter from './Filters/MeasuringPointFilter'
import MeasuringDocumentFilter from './Filters/MeasuringDocumentFilter'
import DocumentNumberFilter from './Filters/DocumentNumberFilter'

function Filter(props) {
  const { dataFilter, handleChange } = props;

  return (
    <div style={{ padding: 16, width: '100%' }}>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Filter
        </AccordionSummary>
        <AccordionDetails>
        <Grid
          container
          spacing={0}
        >
          <Grid item xs={12}>
            <FormControl style={{ margin: 8, minWidth: 200 }}>
              <InputLabel htmlFor="filter-equipment-code">Equipment Code</InputLabel>
              <Select
                value={dataFilter.equipmentCode}
                onChange={handleChange}
                inputProps={{
                  name: 'filter-equipment-code',
                  id: 'filter-equipment-code',
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
          </Grid>

          <Grid item xs={12}>
            <NrpFilter dataFilter={dataFilter} handleChange={handleChange} />
          </Grid>

          <Grid item xs={12}>
            <EquipmentNumberFilter dataFilter={dataFilter} handleChange={handleChange} />
          </Grid>

          <Grid item xs={12}>
            <MeasuringPointFilter dataFilter={dataFilter} handleChange={handleChange} />
          </Grid>

          <Grid item xs={12}>
            <MeasuringDocumentFilter dataFilter={dataFilter} handleChange={handleChange} />
          </Grid>

          <Grid item xs={12}>
            <DocumentNumberFilter
              dataFilter={dataFilter}
              handleChange={handleChange}
            />
          </Grid>
        </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

export default Filter;
