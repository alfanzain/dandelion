import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Filter from './Filter';
import MainTable from './MainTable';

const data = [
  {
    measurement_doc: '-',
    measuring_point: '-',
    document_number: '-',
    equipment_number: '-',
    equipment_desc: '-',
    equipment_code: '-',
    counter_reading: '-',
    unit: '-',
    nrp: '-',
    created_by: '-',
    created_date: '-',
    created_time: '-',
  }
];

function TemuanP2h() {
  const title = 'Temuan P2H';
  const description = '';
  const [dataFilter, setDataFilter] = useState({
    company: '',
    project: '',
    equipmentCode: '',
    createdDate: '',
    documentDate: '',
    nrp: '',
    documentNumber: '',
    approval1: '',
    approval2: '',
    approval3: '',
  });

  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  let dataTable = [];

  data.forEach(items => {
    let item = [];

    for (const property in items) {
      item.push(items[property]);
    }

    dataTable.push(item);
  });

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Paper style={{ paddingTop: 32, paddingBottom: 32 }}>
        <Grid
          container
          spacing={0}
        >
          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={8}>
              <Filter 
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
          >
            <Grid item xs={12}>
              <MainTable 
                data={dataTable}
              />
            </Grid>
          </Grid>
        </Grid> 
      </Paper>
    </div>
  );
}

export default TemuanP2h;
