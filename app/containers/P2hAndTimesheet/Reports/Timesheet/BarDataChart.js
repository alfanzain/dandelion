import React, { useState, useEffect } from "react";
import { Bar } from "react-chartjs-2";
import { teal, grey } from '@material-ui/core/colors';

function BarDataChart(props) {
  const { handleClick, data } = props;
  let [projectCode, setProjectCode] = useState(null);

  useEffect(() => {
    handleClick(projectCode);
  }, [projectCode]);

  return (
    <div>
      <Bar
        data={data}
        width={800}
        height={200}
        options={{
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true,
                  stepSize: 1,
                },
              },
            ],
          },
          legend: {
            display: false,
          },
          onClick: (e, activeEls) => {
            // console.log(data.datasets[0].borderWidth);
            // console.log(activeEls[0]?._index);

            // data.datasets[0].borderWidth[activeEls[0]?._index] = 2;
            
            setProjectCode(activeEls[0]?._chart.data.labels[activeEls[0]._index]);
          }
        }}
      />
    </div>
  );
}

export default BarDataChart;
