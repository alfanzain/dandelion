import React from 'react';
import PropTypes from 'prop-types';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import ProjectFilter from './Filters/ProjectFilter';
import EquipmentCodeFilter from './Filters/EquipmentCodeFilter';
import EquipmentNumberFilter from './Filters/EquipmentNumberFilter';
import DocumentDateFilter from './Filters/DocumentDateFilter';
import NrpFilter from './Filters/NrpFilter';
import DocumentNumberFilter from './Filters/DocumentNumberFilter';
import { sortArrayOfString, sortArrayOfStringOfObject, makeSetOfArray, makeSetOfArrayOfObject } from '../../../../utils/array.js';

function Filter(props) {
  const {
    dataFilter,
    handleChange,
    handleEquipmentNumberChange,
    handleNrpChange,
    handleDocumentNumberChange,
    data,
  } = props;

  let projectItems = sortArrayOfStringOfObject(
    makeSetOfArrayOfObject(data, 'project_code', 'project_name'),
    'value'
  );

  let equipmentCodeItems = sortArrayOfString(
    makeSetOfArray(data, 'equipment_code'),
    'value'
  );

  return (
    <div style={{ padding: 16, width: '100%' }}>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Filter
        </AccordionSummary>
        <AccordionDetails>
        <Grid
          container
          spacing={0}
        >
          <Grid item xs={12}>
            <ProjectFilter 
              dataFilter={dataFilter.project} 
              handleChange={handleChange} 
              projectItems={projectItems} 
            />

            <EquipmentCodeFilter 
              dataFilter={dataFilter.equipmentCode} 
              handleChange={handleChange} 
              equipmentCodeItems={equipmentCodeItems} 
            />

            <Grid item xs={12}>
              <EquipmentNumberFilter 
                dataFilter={dataFilter.equipmentNumber} 
                handleChange={handleEquipmentNumberChange} 
              />
            </Grid>

            <Grid item xs={12}>
              <DocumentDateFilter dataFilter={dataFilter} handleChange={handleChange} />
            </Grid>

            <Grid item xs={12}>
              <NrpFilter 
                dataFilter={dataFilter.nrp} 
                handleChange={handleNrpChange} 
              />
            </Grid>

            <Grid item xs={12}>
              <DocumentNumberFilter 
                dataFilter={dataFilter.documentNumber} 
                handleChange={handleDocumentNumberChange} 
              />
            </Grid>
          </Grid>
        </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

export default Filter;
