import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import Filter from './Filter';
import BarDataChart from './BarDataChart';
import MainTable from './MainTable';

import { generateSimpleTableData, generateCountGroupedLabelChartData } from '../../../../utils/array.js';
import { createDummyData } from './dummy-data.js';

const REPORT_MODE_CHART_AND_TABLE_CODE = 1;
const REPORT_MODE_TABLE_CODE = 2;
const REPORT_MODE_CHART_CODE = 3;

const data = createDummyData();

function Timesheet() {
  const title = 'Timesheet';
  const description = '';
  const [dataFilter, setDataFilter] = useState({
    project: '',
    equipmentCode: '',
    equipmentNumber: {
      from: '',
      to: ''
    },
    nrp: {
      from: '',
      to: ''
    },
    documentNumber: {
      from: '',
      to: ''
    },
    selectedSegmentProjectCode: null,
  });
  const [reportMode, setReportMode] = useState(REPORT_MODE_CHART_AND_TABLE_CODE);
  let [dataTable, setDataTable] = useState([]);
  let [dataChart, setDataChart] = useState([]);

  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  const handleEquipmentNumberChange = (event) => {
    setDataFilter({
      ...dataFilter,
      equipmentNumber: {
        ...dataFilter.equipmentNumber,
        [event.target.name]: event.target.value
      }
    });
  };

  const handleNrpChange = (event) => {
    setDataFilter({
      ...dataFilter,
      nrp: {
        ...dataFilter.nrp,
        [event.target.name]: event.target.value
      }
    });
  };
  
  const handleDocumentNumberChange = (event) => {
    setDataFilter({
      ...dataFilter,
      documentNumber: {
        ...dataFilter.documentNumber,
        [event.target.name]: event.target.value
      }
    });
  };

  const handleSegmentChartClick = (code) => {
    setDataFilter({
      ...dataFilter,
      selectedSegmentProjectCode: code
    });
  };

  useEffect(() => {
    let filteredData = data.filter(item => 
      (item.project_code === dataFilter.project || dataFilter.project === '') &&
      (item.equipment_code === dataFilter.equipmentCode || dataFilter.equipmentCode === '') &&
      (
        parseInt(item.equipment_no) >= parseInt(dataFilter.equipmentNumber.from) && parseInt(item.equipment_no) <= parseInt(dataFilter.equipmentNumber.to)
        || (dataFilter.equipmentNumber.from === '' || dataFilter.equipmentNumber.to === '')
      ) && 
      (
        parseInt(item.nrp) >= parseInt(dataFilter.nrp.from) && parseInt(item.nrp) <= parseInt(dataFilter.nrp.to)
        || (dataFilter.nrp.from === '' || dataFilter.nrp.to === '')
      ) &&
      (
        parseInt(item.document_no) >= parseInt(dataFilter.documentNumber.from) && parseInt(item.document_no) <= parseInt(dataFilter.documentNumber.to)
        || (dataFilter.documentNumber.from === '' || dataFilter.documentNumber.to === '')
      )
    );
    let filteredDataTableSegmentProject = null;

    if (dataFilter.selectedSegmentProjectCode) {
      filteredDataTableSegmentProject = filteredData.filter(item => item.project_code === dataFilter.selectedSegmentProjectCode);
    }

    setDataTable(
      generateSimpleTableData(filteredDataTableSegmentProject ?? filteredData)
    );

    setDataChart(
      generateCountGroupedLabelChartData(filteredData, 'project_code')
    );
  }, [dataFilter]);

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Paper style={{ paddingTop: 32, paddingBottom: 32 }}>
        <Grid
          container
          spacing={0}
        >
          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={8}>
              <Filter 
                data={data}
                dataFilter={dataFilter}
                handleChange={handleChange}
                handleEquipmentNumberChange={handleEquipmentNumberChange}
                handleNrpChange={handleNrpChange}
                handleDocumentNumberChange={handleDocumentNumberChange}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item xs={8}>
              { 
                [
                  REPORT_MODE_CHART_CODE, 
                  REPORT_MODE_CHART_AND_TABLE_CODE
                ].includes(reportMode) 
                  ? <BarDataChart 
                      data={dataChart}
                      handleClick={handleSegmentChartClick}
                    /> 
                  : null 
              }
            </Grid>
            <Grid 
              item 
              xs={2}
            >
              <Paper elevation={0}>
                <Grid
                  container
                  spacing={2}
                  direction="column"
                  justifyContent="flex-start"
                  alignItems="flex-end"
                >
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_CHART_AND_TABLE_CODE)}
                      color={
                        [
                          REPORT_MODE_CHART_AND_TABLE_CODE
                        ].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Chart & Table
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_CHART_CODE)}
                      color={
                        [
                          REPORT_MODE_CHART_CODE
                        ].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Chart Only
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      onClick={() => setReportMode(REPORT_MODE_TABLE_CODE)}
                      color={
                        [
                          REPORT_MODE_TABLE_CODE
                        ].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      Table Only
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
          >
            <Grid item xs={12}>
              { 
                [
                  REPORT_MODE_TABLE_CODE, 
                  REPORT_MODE_CHART_AND_TABLE_CODE
                ].includes(reportMode) 
                  ? <MainTable 
                      data={dataTable}
                    /> 
                  : null 
              }
            </Grid>
          </Grid>
        </Grid> 
      </Paper>
    </div>
  );
}

export default Timesheet;
