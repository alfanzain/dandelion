import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
});

function EquipmentCodeFilter(props) {
  const { dataFilter, handleChange, equipmentCodeItems } = props;

  return (
    <FormControl style={{ margin: 8, minWidth: 200 }}>
      <InputLabel htmlFor="filter-equipment-code">Equipment Code</InputLabel>
      <Select
        value={dataFilter}
        onChange={handleChange}
        inputProps={{
          name: 'equipmentCode',
          id: 'filter-equipment-code',
        }}
      >
        <MenuItem value="">
          <em>All</em>
        </MenuItem>
        {
          equipmentCodeItems.map(item => (<MenuItem value={item}>{item}</MenuItem>))
        }
      </Select>
    </FormControl>
  );
}

EquipmentCodeFilter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EquipmentCodeFilter);
