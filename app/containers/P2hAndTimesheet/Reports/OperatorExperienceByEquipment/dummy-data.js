const persons = [
  {
    code: "899999991",
    name: "Burhan",
    email: "burhan@gmail.com",
  },
  {
    code: "899999992",
    name: "Asep",
    email: "asep@gmail.com",
  },
  {
    code: "899999993",
    name: "Udin",
    email: "udin@gmail.com",
  },
  {
    code: "899999994",
    name: "Septian",
    email: "septian@gmail.com",
  },
  {
    code: "899999995",
    name: "Rudi",
    email: "rudi@gmail.com",
  },
  {
    code: "899999996",
    name: "Gatot",
    email: "gatot@gmail.com",
  },
  {
    code: "899999997",
    name: "Ifan",
    email: "ifan@gmail.com",
  },
  {
    code: "899999998",
    name: "Doso",
    email: "doso@gmail.com",
  },
  {
    code: "899999999",
    name: "Timbul",
    email: "timbul@gmail.com",
  },
  {
    code: "900000000",
    name: "Sobur Ah",
    email: "soburah@gmail.com",
  },
];
const equipments = [
  {
    code: "EX001",
    number: "10000001",
    description: "Hydraulic Excavator",
    model_no: 'PC2-8MO',
  },
  {
    code: "EX002",
    number: "10000002",
    description: "Min Loader",
    model_no: 'S570',
  },
  {
    code: "EX003",
    number: "10000003",
    description: "Compactor",
    model_no: 'BW211D-40',
  },
  {
    code: "EX004",
    number: "10000004",
    description: "Hydraulic Drilling Rig",
    model_no: 'PC2-DRL40',
  },
]
const plantOrigins = ["W001"];
const companyCodes = ["ACST"];
const positions = ["Heavy Equipment Operator"];
const phoneNumbers = ["123"];
const projects = [
  {
    code: "B001",
    name: "Thamrin 9",
  },
  {
    code: "B007",
    name: "Apartment Cleon",
  },
];

const numberValue = (max, times) => {
  return Math.floor(Math.random() * max) * times;
};

const COUNT_DATA = 10;

export function createDummyData() {
  let search = window.location.search;
  let params = new URLSearchParams(search);
  let nrpQuery = params.get('nrp');

  let data = [];
  const person = persons.find(person => person.code === nrpQuery)

  for (let i = 0; i < COUNT_DATA; i++) {
    let equipment = equipments[numberValue(equipments.length, 1)];

    data.push({
      nrp: person.code,
      full_name: person.name,
      equipment_number: equipment.number,
      equipment_description: equipment.description,
      model_no: equipment.model_no,
      equipment_code: equipment.code,
      jt: numberValue(10, 100),
      opr: numberValue(10, 100),
      bsc: numberValue(10, 100),
      bus: numberValue(10, 100),
      stb: numberValue(10, 100),
      opt: numberValue(10, 100),
      pso: numberValue(10, 100),
      ism: numberValue(10, 100),
      pph: numberValue(10, 100),
    });
  }

  return data;
}
