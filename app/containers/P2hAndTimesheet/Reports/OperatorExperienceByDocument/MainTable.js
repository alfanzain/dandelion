import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MUIDataTable from 'mui-datatables';
import { lighten, darken } from '@material-ui/core/styles';

const styles = theme => ({
  table: {
    '& > div': {
      // overflow: 'auto'
    },
    '& table': {
      '& tbody tr:hover': {
        background: theme.palette.type === 'dark' ? darken(theme.palette.primary.light, 0.8) : lighten(theme.palette.primary.light, 0.5)
      },
      '& tr': {
        height: 24,
        '& td, th': {
          padding: '4px 10px',
          fontSize: 12
        }
      },
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          // overflow: 'hidden',
          // textOverflow: 'ellipsis'
        }
      },
    }
  }
});

function MainTable(props) {
  const columns = [
    {
      name: "Document Number",
      options: {
        filter: true,
      }
    },
    {
      name: "NRP",
      options: {
        filter: true,
      }
    },
    {
      name: "Full Name",
      options: {
        filter: true,
      }
    },
    {
      name: "Document Date",
      options: {
        filter: true,
      }
    },
    {
      name: "Created Date",
      options: {
        filter: true,
      }
    },
    {
      name: "Created Time",
      options: {
        filter: true,
      }
    },
    {
      name: "Equipment Number",
      options: {
        filter: true,
      }
    },
    {
      name: "Equipment Description",
      options: {
        filter: true,
      }
    },
    {
      name: "Model No.",
      options: {
        filter: true,
      }
    },
    {
      name: "Equipment Code",
      options: {
        filter: true,
      }
    },
    {
      name: "JT",
      options: {
        filter: true,
      }
    },
    {
      name: "OPR",
      options: {
        filter: true,
      }
    },
    {
      name: "BSC",
      options: {
        filter: true,
      }
    },
    {
      name: "BUS",
      options: {
        filter: true,
      }
    },
    {
      name: "STB",
      options: {
        filter: true,
      }
    },
    {
      name: "OPT",
      options: {
        filter: true,
      }
    },
    {
      name: "PSO",
      options: {
        filter: true,
      }
    },
    {
      name: "ISM",
      options: {
        filter: true,
      }
    },
    {
      name: "PPH",
      options: {
        filter: true,
      }
    },
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    rowsPerPage: 10,
    page: 0,
    selectableRows: 'none'
  };

  const { classes, data } = props;

  return (
    <Fragment>
      <div className={classes.table}>
        <MUIDataTable
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    </Fragment>
  );
}

MainTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainTable);
