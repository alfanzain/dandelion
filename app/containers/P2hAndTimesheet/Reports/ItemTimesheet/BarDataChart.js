import React from 'react';
import { Bar } from 'react-chartjs-2';

function BarDataChart(props) {
  const { data } = props;

  return (
    <div>
      <Bar
        data={data}
        width={800}
        height={200}
        options={{
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true,
                      stepSize: 1,
                  }
              }]
          }
      }}
      />
    </div>
  );
}

export default BarDataChart;