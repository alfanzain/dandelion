import React, { useState } from 'react';
import { Helmet } from 'react-helmet';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import Filter from './Filter';
import MainTable from './MainTable';

const data = [
  {
    company: '-',
    project_code: '-',
    document_no: '-',
    document_date: '-',
    equipment_no: '-',
    equipment_desc: '-',
    equipment_code: '-',
    model: '-',
    manufacturer: '-',
    shift: '-',
    counter_reading_start: '-',
    counter_reading_end: '-',
    unit: '-',
    nrp: '-',
    full_name: '-',
    job_key: '-',
    start_operation: '-',
    finish_operation: '-',
    activity: '-',
    qty: '-',
    uo_m: '-',
    hm_km: '-',
    start_activity: '-',
    finish_activity: '-',
    total_time_activity: '-',
    adjusted_time_activity: '-',
    approval_1: '-',
    name_approval_1: '-',
    date_approval_1: '-',
    time_approval_1: '-',
    approval_2: '-',
    name_approval_2: '-',
    date_approval_2: '-',
    time_approval_2: '-',
    notes: '-',
    created_by: '-',
    created_date: '-',
    created_time: '-',
    last_changed_by: '-',
    last_change_date: '-',
    last_change_time: '-',
    status: '-',
  },
];

function Timesheet() {
  const title = 'Timesheet';
  const description = '';
  const [dataFilter, setDataFilter] = useState({
    company: '',
    project: '',
    equipmentCode: '',
    documentDate: '',
    nrp: '',
    equipmentNumber: '',
  });

  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  let dataTable = [];

  data.forEach(items => {
    let item = [];

    for (const property in items) {
      item.push(items[property]);
    }

    dataTable.push(item);
  });

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Paper style={{ paddingTop: 32, paddingBottom: 32 }}>
        <Grid
          container
          spacing={0}
        >
          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={8}>
              <Filter 
                dataFilter={dataFilter}
                handleChange={handleChange}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
          >
            <Grid item xs={12}>
              <MainTable 
                data={dataTable}
              />
            </Grid>
          </Grid>
        </Grid> 
      </Paper>
    </div>
  );
}

export default Timesheet;
