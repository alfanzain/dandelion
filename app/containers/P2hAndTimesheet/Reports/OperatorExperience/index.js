import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import ShowChart from '@material-ui/icons/ShowChart';
import GridOn from '@material-ui/icons/GridOn';

import Filter from './Filter';
import BarDataChart from './BarDataChart';
import MainTable from './MainTable';

import { generateSimpleTableData, generateSimpleChartData } from '../../../../utils/array.js';
import { createDummyData } from './dummy-data.js';

const REPORT_MODE_CHART_AND_TABLE_CODE = 1;
const REPORT_MODE_TABLE_CODE = 2;
const REPORT_MODE_CHART_CODE = 3;

const data = createDummyData();

function OperatorExperience() {
  const title = 'Report Operator Experience';
  const description = '';
  const [dataFilter, setDataFilter] = useState({
    company: '',
    project: '',
    equipmentCode: '',
    documentDate: '',
    nrp: {
      from: '',
      to: ''
    },
    equipmentNumber: '',
  });
  let [dataTable, setDataTable] = useState([]);
  let [dataChart, setDataChart] = useState([]);
  const [reportMode, setReportMode] = useState(REPORT_MODE_CHART_AND_TABLE_CODE);

  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  const handleNrpChange = (event) => {
    setDataFilter({
      ...dataFilter,
      nrp: {
        ...dataFilter.nrp,
        [event.target.name]: event.target.value
      }
    });
  };

  useEffect(() => {
    let filteredData = data.filter(item => 
      (item.company_code === dataFilter.company || dataFilter.company === '') &&
      (item.last_project === dataFilter.project || dataFilter.project === '') &&
      (
        parseInt(item.personal_number) >= parseInt(dataFilter.nrp.from) && parseInt(item.personal_number) <= parseInt(dataFilter.nrp.to)
        || (dataFilter.nrp.from === '' || dataFilter.nrp.to === '')
      )
    );

    setDataTable(
      generateSimpleTableData(filteredData)
    );

    setDataChart(
      generateSimpleChartData(filteredData, 'full_name', 'jt')
    );
  }, [dataFilter]);

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Paper style={{ paddingTop: 32, paddingBottom: 32 }}>
        <Grid
          container
          spacing={0}
        >
          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={8}>
              <Filter 
                data={data}
                dataFilter={dataFilter}
                handleChange={handleChange}
                handleNrpChange={handleNrpChange}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item xs={8}>
              { 
                [
                  REPORT_MODE_CHART_CODE, 
                  REPORT_MODE_CHART_AND_TABLE_CODE
                ].includes(reportMode) 
                  ? <BarDataChart 
                      data={ dataChart }
                    /> 
                  : null 
              }
            </Grid>
            <Grid 
              item 
              xs={2}
            >
              <Paper elevation={0}>
                <Grid
                  container
                  spacing={2}
                  direction="column"
                  justifyContent="flex-start"
                  alignItems="flex-end"
                >
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      size="small"
                      onClick={() => setReportMode(REPORT_MODE_CHART_AND_TABLE_CODE)}
                      color={
                        [
                          REPORT_MODE_CHART_AND_TABLE_CODE
                        ].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      <GridOn />
                      <ShowChart />
                      <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
                        Chart & Table
                      </Box>
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      size="small"
                      onClick={() => setReportMode(REPORT_MODE_CHART_CODE)}
                      color={
                        [
                          REPORT_MODE_CHART_CODE
                        ].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      <ShowChart />
                      <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
                        Chart Only
                      </Box>
                    </Button>
                  </Grid>
                  <Grid 
                    item 
                    xs={12}
                  >
                    <Button 
                      variant="contained"
                      size="small"
                      onClick={() => setReportMode(REPORT_MODE_TABLE_CODE)}
                      color={
                        [
                          REPORT_MODE_TABLE_CODE
                        ].includes(reportMode) 
                        ? 'primary'
                        : 'default'
                      }
                    >
                      <GridOn />
                      <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
                        Table Only
                      </Box>
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={0}
          >
            <Grid item xs={12}>
              { 
                [
                  REPORT_MODE_TABLE_CODE, 
                  REPORT_MODE_CHART_AND_TABLE_CODE
                ].includes(reportMode) 
                  ? <MainTable 
                      data={dataTable}
                    /> 
                  : null 
              }
            </Grid>
          </Grid>
        </Grid> 
      </Paper>
    </div>
  );
}

export default OperatorExperience;
