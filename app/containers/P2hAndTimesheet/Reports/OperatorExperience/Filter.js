import React from 'react';
import PropTypes from 'prop-types';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import FormControl from '@material-ui/core/FormControl';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import CompanyFilter from './Filters/CompanyFilter';
import ProjectFilter from './Filters/ProjectFilter';
import DocumentDateFilter from './Filters/DocumentDateFilter';
import NrpFilter from './Filters/NrpFilter';
import { sortArrayOfString, sortArrayOfStringOfObject, makeSetOfArray, makeSetOfArrayOfObject } from '../../../../utils/array.js';

function Filter(props) {
  const { dataFilter, handleChange, handleNrpChange, data } = props;

  let companyItems = sortArrayOfString(
    makeSetOfArray(data, 'company_code')
  );

  let projectItems = sortArrayOfStringOfObject(
    makeSetOfArrayOfObject(data, 'last_project', 'project_name'),
    'value'
  );

  return (
    <div style={{ padding: 16, width: '100%' }}>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Filter
        </AccordionSummary>
        <AccordionDetails>
        <Grid
          container
          spacing={0}
        >
          <Grid item xs={12}>
            <CompanyFilter 
              dataFilter={dataFilter.company} 
              handleChange={handleChange} 
              companyItems={companyItems} 
            />

            <ProjectFilter 
              dataFilter={dataFilter.project} 
              handleChange={handleChange} 
              projectItems={projectItems} 
            />

            <FormControl style={{ margin: 8, minWidth: 200 }}>
              <InputLabel htmlFor="filter-equipment-code">Equipment Code (x)</InputLabel>
              <Select
                value={dataFilter.equipmentCode}
                onChange={handleChange}
                inputProps={{
                  name: 'filter-equipment-code',
                  id: 'filter-equipment-code',
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>

            <Grid item xs={12}>
              <DocumentDateFilter 
                dataFilter={dataFilter} 
                handleChange={handleChange} 
              />
            </Grid>

            <Grid item xs={12}>
              <NrpFilter 
                dataFilter={dataFilter.nrp} 
                onChange={handleNrpChange} 
              />
            </Grid>

            <FormControl style={{ margin: 8, minWidth: 200 }}>
              <InputLabel htmlFor="filter-equipment-number">Equipment Number (x)</InputLabel>
              <Select
                value={dataFilter.equipmentNumber}
                onChange={handleChange}
                inputProps={{
                  name: 'filter-equipment-number',
                  id: 'filter-equipment-number',
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
          </Grid>
        </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

export default Filter;
