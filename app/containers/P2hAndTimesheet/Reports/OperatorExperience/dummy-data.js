const persons = [
  {
    code: "899999991",
    name: "Burhan",
    email: "burhan@gmail.com",
  },
  {
    code: "899999992",
    name: "Asep",
    email: "asep@gmail.com",
  },
  {
    code: "899999993",
    name: "Udin",
    email: "udin@gmail.com",
  },
  {
    code: "899999994",
    name: "Septian",
    email: "septian@gmail.com",
  },
  {
    code: "899999995",
    name: "Rudi",
    email: "rudi@gmail.com",
  },
  {
    code: "899999996",
    name: "Gatot",
    email: "gatot@gmail.com",
  },
  {
    code: "899999997",
    name: "Ifan",
    email: "ifan@gmail.com",
  },
  {
    code: "899999998",
    name: "Doso",
    email: "doso@gmail.com",
  },
  {
    code: "899999999",
    name: "Timbul",
    email: "timbul@gmail.com",
  },
  {
    code: "900000000",
    name: "Sobur Ah",
    email: "soburah@gmail.com",
  },
];
const plantOrigins = ["W001"];
const companyCodes = ["ACST", "ASTR"];
const positions = ["Heavy Equipment Operator"];
const phoneNumbers = ["123"];
const projects = [
  {
    code: "B001",
    name: "Thamrin 9",
  },
  {
    code: "B007",
    name: "Apartment Cleon",
  },
];

const numberValue = (max, times) => {
  return Math.floor(Math.random() * max) * times;
};

export function createDummyData() {
  return persons.map((person, index) => {
    let project = projects[numberValue(projects.length, 1)];
    let company = companyCodes[numberValue(companyCodes.length, 1)];

    return {
      created_date: "2020-02-24",
      company_code: company,
      plant_origin: plantOrigins[0],
      personal_number: person.code,
      full_name: person.name,
      position: positions[0],
      phone: phoneNumbers[0],
      email: person.email,
      last_project: project.code,
      project_name: project.name,
      last_change_date: "2021-02-18",
      status: numberValue(2, 1) === 0 ? 'active' : 'inactive',
      jt: numberValue(10, 100),
      opr: numberValue(10, 100),
      bsc: numberValue(10, 100),
      bus: numberValue(10, 100),
      stb: numberValue(10, 100),
      opt: numberValue(10, 100),
      pso: numberValue(10, 100),
      ism: numberValue(10, 100),
      pph: numberValue(10, 100),
    };
  });
}
