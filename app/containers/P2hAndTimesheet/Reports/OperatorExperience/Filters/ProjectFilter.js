import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
});

function ProjectFilter(props) {
  const { dataFilter, handleChange, projectItems } = props;

  return (
    <FormControl style={{ margin: 8, minWidth: 200 }}>
      <InputLabel htmlFor="filter-project">Project</InputLabel>
      <Select
        value={dataFilter}
        onChange={handleChange}
        inputProps={{
          name: 'project',
          id: 'filter-project',
        }}
      >
        <MenuItem value="">
          <em>All</em>
        </MenuItem>
        {
          projectItems.map(item => (<MenuItem value={item.value}>{item.text}</MenuItem>))
        }
      </Select>
    </FormControl>
  );
}

ProjectFilter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProjectFilter);
