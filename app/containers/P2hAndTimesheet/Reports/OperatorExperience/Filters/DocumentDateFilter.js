import React, { useState } from 'react';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import Type from 'dan-styles/Typography.scss';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import idLocale from 'date-fns/locale/id';
import enLocale from 'date-fns/locale/en-US';

const localeMap = {
  en: enLocale,
  id: idLocale,
};

export default function DocumentDateFilter(props) {
  const { dataFilter, handleChange } = props;
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentLocale, setCurrentLocale] = useState('id');
  const locale = localeMap[currentLocale];
  
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const handleMenuOpen = (event) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const selectLocale = (selectedLocale) => {
    setCurrentLocale(selectedLocale);
    setAnchorEl(null);
  };

  return (
    <Grid
      container
      spacing={0}
      alignItems='flex-end'
      style={{ margin: 8 }}
    >
      <Grid item lg={2} md={2} sm={5} xs={5}>
        <MuiPickersUtilsProvider 
          style={{ margin: 8 }}
          utils={MomentUtils}
        >
          <DatePicker
            label="Document Date (x)"
            format="DD/MM/yyyy"
            value={selectedDate}
            onChange={handleDateChange}
            animateYearScrolling={false}
          />
        </MuiPickersUtilsProvider>
      </Grid>
      <Grid item lg={1} md={1} sm={2} xs={2}>
        <Typography variant="subtitle1" className={Type.textCenter} gutterBottom>
          <span className={Type.bold}>To</span>
        </Typography>
      </Grid>
      <Grid item lg={2} md={2} sm={5} xs={5}>
        <MuiPickersUtilsProvider 
          utils={MomentUtils}
        >
          <DatePicker
            format="DD/MM/yyyy"
            value={selectedDate}
            onChange={handleDateChange}
            animateYearScrolling={false}
          />
        </MuiPickersUtilsProvider>
      </Grid>
    </Grid>
  )
}