import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Type from 'dan-styles/Typography.scss';

const styles = theme => ({
  demo: {
    height: 'auto',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

function NrpFilter(props) {
  const { onChange } = props;

  return (
    <Grid
      container
      spacing={0}
      alignItems='flex-end'
      style={{ margin: 8 }}
    >
      <Grid item xs={2}>
        <FormControl>
          <InputLabel htmlFor="filter-nrp-from">NRP</InputLabel>
          <Input 
            id="filter-nrp-from" 
            name="from"
            onChange={onChange} 
          />
        </FormControl>
      </Grid>
      <Grid item xs={1}>
        <Typography variant="subtitle1" className={Type.textCenter} gutterBottom>
          <span className={Type.bold}>To</span>
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <FormControl>
          <Input 
            id="filter-nrp-to" 
            name="to"
            onChange={onChange} 
          />
        </FormControl>
      </Grid>
    </Grid>
  );
}

NrpFilter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NrpFilter);
