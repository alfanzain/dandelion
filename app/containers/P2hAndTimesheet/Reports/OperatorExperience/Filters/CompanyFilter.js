import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
});

function CompanyFilter(props) {
  const { dataFilter, handleChange, companyItems } = props;

  return (
    <FormControl style={{ margin: 8, minWidth: 200 }}>
      <InputLabel htmlFor="filter-company">Company</InputLabel>
      <Select
        value={dataFilter}
        onChange={handleChange}
        inputProps={{
          name: 'company',
          id: 'filter-company',
        }}
      >
        <MenuItem value="">
          <em>All</em>
        </MenuItem>
        {
          companyItems.map(item => (<MenuItem value={item}>{item}</MenuItem>))
        }
      </Select>
    </FormControl>
  );
}

CompanyFilter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CompanyFilter);
