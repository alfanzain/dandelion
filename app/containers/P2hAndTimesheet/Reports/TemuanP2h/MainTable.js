import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import { lighten, darken } from "@material-ui/core/styles";

const styles = (theme) => ({
    table: {
        "& > div": {
            // overflow: 'auto'
        },
        "& table": {
            "& tbody tr:hover": {
                background:
                    theme.palette.type === "dark"
                        ? darken(theme.palette.primary.light, 0.8)
                        : lighten(theme.palette.primary.light, 0.5),
            },
            "& tr": {
                height: 24,
                "& td, th": {
                    padding: "4px 10px",
                    fontSize: 12,
                },
            },
            "& td": {
                wordBreak: "keep-all",
            },
            [theme.breakpoints.down("md")]: {
                "& td": {
                    height: 60,
                    // overflow: 'hidden',
                    // textOverflow: 'ellipsis'
                },
            },
        },
    },
});

const columnNames = [
    'Document Date',
    'Project Code',
    'Project Name',
    'Document No.',
    'Item',
    'Shift',
    'Equipment Type',
    'Equipment Code',
    'Equipment Desc.',
    'Component Group',
    'Before/After',
    'Problem',
    'Remark',
    'Critical',
    'Operation',
    'Condition',
    'Status',
    'Malfunction Date',
    'Malfunction Time',
    'Required Start Date',
    'Required Start Time',
    'Notification',
    'Order',
    'Work Start Date',
    'Work Start Time',
    'Work Finish Date',
    'Work Finish Time',
    'HM/KM',
    'General Problem',
    'Object Item',
    'Damage',
    'Cause',
    'Tasks',
    'Activities',
    'Action',
    'PIC',
    'Approval 1',
    'Name 1',
    'Date Approval 1',
    'Time Approval 1',
    'Leadtime Repair (Hours)',
    'Leadtime Repair (Days)',
    'NRP',
    'Created By',
    'Created Date',
    'Created Time',
    'Last Changed By',
    'Last Change Date',
    'Last Change Time',
    'Status',
] 

function createColumn() {
    return columnNames.map(column => {
        return {
            name: column,
            options: {
                filter: true,
            },
        }
    });
}

function MainTable(props) {
    const columns = createColumn();

    const options = {
        filterType: "dropdown",
        responsive: "vertical",
        rowsPerPage: 10,
        page: 0,
        selectableRows: false,
    };

    const { classes, data } = props;

    return (
        <Fragment>
            <div className={classes.table}>
                <MUIDataTable data={data} columns={columns} options={options} />
            </div>
        </Fragment>
    );
}

MainTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainTable);
