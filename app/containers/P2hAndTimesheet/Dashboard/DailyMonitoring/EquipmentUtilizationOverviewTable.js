import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { lighten, darken } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = (theme) => ({
  table: {
    "& > div": {
      // overflow: 'auto'
    },
    "& table": {
      "& tbody tr:hover": {
        background:
          theme.palette.type === "dark"
            ? darken(theme.palette.primary.light, 0.8)
            : lighten(theme.palette.primary.light, 0.5),
      },
      "& tr": {
        height: 24,
        "& td, th": {
          padding: "0px 0px",
          fontSize: 12,
        },
      },
      "& td": {
        wordBreak: "keep-all",
      },
      [theme.breakpoints.down("md")]: {
        "& td": {
          height: 60,
          // overflow: 'hidden',
          // textOverflow: 'ellipsis'
        },
      },
    },
  },
});

function EquipmentUtilizationOverviewTable(props) {
  const columns = [
    {
      name: 'Equipment Type',
      options: {
        filter: true
      }
    },
    {
      name: 'Equipment Type Description',
      options: {
        filter: true,
      }
    },
    {
      name: 'Total',
      options: {
        filter: true,
      }
    },
    {
      name: '',
      options: {
        filter: false,
        customBodyRender: (value) => (
          <LinearProgress variant="determinate" color="primary" value={value} />
        ),
        setCellProps: () => ({ style: { minWidth: "200px", maxWidth: "200px" }}),
      }
    },
  ];

  const data = [
    ['Gabby George', 'Business Analyst', 30, 30],
    ['Aiden Lloyd', 'Business Consultant', 55, 55],
    ['Jaden Collins', 'Attorney', 27, 27],
    ['Franky Rees', 'Business Analyst', 90, 90],
    ['Aaren Rose', 'Business Consultant', 28, 28],
    ['Blake Duncan', 'Business Management Analyst', 65, 65],
    ['Frankie Parry', 'Agency Legal Counsel', 71, 71],
    ['Lane Wilson', 'Commercial Specialist', 19, 19],
    ['Robin Duncan', 'Business Analyst', 20, 20],
    ['Mel Brooks', 'Business Consultant', 89, 89],
    ['Harper White', 'Attorney', 52, 52],
    ['Kris Humphrey', 'Agency Legal Counsel', 80, 80],
    ['Frankie Long', 'Industrial Analyst', 31, 31],
    ['Brynn Robbins', 'Business Analyst', 22, 22],
    ['Justice Mann', 'Business Consultant', 76, 76],
    ['Addison Navarro', 'Business Management Analyst', 50, 50],
    ['Jesse Welch', 'Agency Legal Counsel', 28, 28],
    ['Eli Mejia', 'Commercial Specialist', 65, 65],
    ['Gene Leblanc', 'Industrial Analyst', 100, 100],
    ['Danny Leon', 'Computer Scientist', 60, 60],
    ['Lane Lee', 'Corporate Counselor', 52, 52],
    ['Jesse Hall', 'Business Analyst', 44, 44],
    ['Danni Hudson', 'Agency Legal Counsel', 37, 37],
    ['Terry Macdonald', 'Commercial Specialist', 39, 39],
    ['Justice Mccarthy', 'Attorney', 26, 26],
    ['Silver Carey', 'Computer Scientist', 10, 10],
    ['Franky Miles', 'Industrial Analyst', 49, 49],
    ['Glen Nixon', 'Corporate Counselor', 15,, ,],
    ['Gabby Strickland', 'Business Process Consultant', 26, 26],
    ['Mason Ray', 'Computer Scientist', 39, 390]
  ];

  const options = {
    filterType: "dropdown",
    responsive: "standard",
    rowsPerPage: 10,
    page: 0,
    selectableRows: false,
  };

  const { classes } = props;

  return (
    <Fragment>
      <div className={classes.table}>
        <MUIDataTable data={data} columns={columns} options={options} />
      </div>
    </Fragment>
  );
}

EquipmentUtilizationOverviewTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EquipmentUtilizationOverviewTable);
