import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import colorfull from 'dan-api/palette/colorfull';
import { CounterWidget } from 'dan-components';
import styles from 'dan-components/Widget/widget-jss';

function Infographic(props) {
  return (
      <Grid container spacing={2}>
        <Grid item xs={6} md={3}>
          <CounterWidget
            color={colorfull[0]}
            start={0}
            end={207}
            duration={3}
            title="Total Operator"
          >
          </CounterWidget>
        </Grid>
        <Grid item xs={6} md={3}>
          <CounterWidget
            color={colorfull[1]}
            start={0}
            end={300}
            duration={3}
            title="Total P2H"
          >
          </CounterWidget>
        </Grid>
        <Grid item xs={6} md={3}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={67}
            duration={3}
            title="Not Yet"
          >
          </CounterWidget>
        </Grid>
        <Grid item xs={6} md={3}>
          <CounterWidget
            color={colorfull[3]}
            start={0}
            end={70}
            duration={3}
            title="P2H Percentage"
          >
          </CounterWidget>
        </Grid>
        <Grid item xs={6} md={3}>
          <CounterWidget
            color={colorfull[3]}
            start={0}
            end={70}
            duration={3}
            title="Projects"
          >
          </CounterWidget>
        </Grid>
      </Grid>
  );
}

Infographic.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Infographic);
