import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { lighten, darken } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";

const styles = (theme) => ({
  table: {
    "& > div": {
      // overflow: 'auto'
    },
    "& table": {
      "& tbody tr:hover": {
        background:
          theme.palette.type === "dark"
            ? darken(theme.palette.primary.light, 0.8)
            : lighten(theme.palette.primary.light, 0.5),
      },
      "& tr": {
        height: 24,
        "& td, th": {
          padding: "4px 10px",
          fontSize: 12,
        },
      },
      "& td": {
        wordBreak: "keep-all",
      },
      [theme.breakpoints.down("md")]: {
        "& td": {
          height: 60,
          // overflow: 'hidden',
          // textOverflow: 'ellipsis'
        },
      },
    },
  },
});

function OperatorDocumentOverviewTable(props) {
  const columns = [
    {
      name: "NRP",
      options: {
        filter: true,
      },
    },
    {
      name: "Full Name",
      options: {
        filter: true,
      },
    },
    {
      name: "Status",
      options: {
        filter: true,
      },
    },
    {
      name: "Document No",
      options: {
        filter: true,
      },
    },
    {
      name: "Created Date",
      options: {
        filter: true,
      },
    },
    {
      name: "Created Time",
      options: {
        filter: true,
      },
    },
  ];

  const options = {
    filterType: "dropdown",
    responsive: "standard",
    rowsPerPage: 10,
    page: 0,
    selectableRows: false,
  };

  const { classes, data } = props;

  return (
    <Fragment>
      <div className={classes.table}>
        <MUIDataTable data={data} columns={columns} options={options} />
      </div>
    </Fragment>
  );
}

OperatorDocumentOverviewTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(OperatorDocumentOverviewTable);
