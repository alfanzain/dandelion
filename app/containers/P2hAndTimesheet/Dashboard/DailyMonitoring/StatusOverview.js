import React from 'react';
import { PapperBlock } from 'dan-components';
import CustomPieChart from './CustomPieChart';

export default function StatusOverview(props) {
  const { data } = props;
  
  return (<PapperBlock
    title="Status Overview"
    icon="ion-ios-stats-outline"
    desc=""
    overflowX
  >
    <div>
      <CustomPieChart
        data={data}
      />
    </div>
  </PapperBlock>);
}
