import React from 'react';
import { PapperBlock } from 'dan-components';
import OperatorDocumentOverviewTable from './OperatorDocumentOverviewTable';

export default function OperatorDocumentOverview(props) {
  const { data } = props;
  
  return (<PapperBlock
    title="Operator Document Overview"
    icon="ion-ios-stats-outline"
    desc=""
    overflowX
  >
    <OperatorDocumentOverviewTable
      data={data}
    />
  </PapperBlock>);
}
