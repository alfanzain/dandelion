import React from 'react';
import { PapperBlock } from 'dan-components';
import BarDataChart from './BarDataChart';

export default function ProjectOverview(props) {
  const { data } = props;
  
  return (<PapperBlock
    title="Project Overview"
    icon="ion-ios-stats-outline"
    desc=""
    overflowX
  >
    <div>
      <BarDataChart
        data={data}
      />
    </div>
  </PapperBlock>);
}
