import React from "react";
import { Bar } from "react-chartjs-2";

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          stepSize: 1,
        },
      },
    ],
  },
  legend: {
    display: false,
  },
};

function BarDataChart(props) {
  const { data } = props;

  return (
    <div>
      <Bar data={data} width={400} height={200} options={options} />
    </div>
  );
}

export default BarDataChart;
