import React from 'react';
import { PapperBlock } from 'dan-components';
import EquipmentUtilizationOverviewTable from './EquipmentUtilizationOverviewTable';

export default function EquipmentUtilizationOverview(props) {
  const { data } = props;
  
  return (<PapperBlock
    title="Operator Document Overview"
    icon="ion-ios-stats-outline"
    desc=""
    overflowX
  >
    <EquipmentUtilizationOverviewTable
      data={data}
    />
  </PapperBlock>);
}
