import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { createTheme } from '@material-ui/core/styles';
import ThemePallete from 'dan-api/palette/themePalette';
import {
  PieChart,
  Pie,
  Sector,
  Legend,
  Cell
} from 'recharts';
import purple from '@material-ui/core/colors/purple';
import red from '@material-ui/core/colors/red';
import pink from '@material-ui/core/colors/pink';
import indigo from '@material-ui/core/colors/indigo';
import blue from '@material-ui/core/colors/blue';
import cyan from '@material-ui/core/colors/cyan';
import teal from '@material-ui/core/colors/teal';

const theme = createTheme(ThemePallete.greenTheme);
const color = ({
  primary: theme.palette.primary.main,
  secondary: theme.palette.secondary.main,
});
const colors = [red[500], pink[500], purple[500], indigo[500], blue[500], cyan[500], teal[500]];

let middlePieChartX = 0;
let middlePieChartY = 0;

const renderActiveShape = props => {
  const RADIAN = Math.PI / 180;
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + ((outerRadius + 10) * cos);
  const sy = cy + ((outerRadius + 10) * sin);
  const mx = cx + ((outerRadius + 30) * cos);
  const my = cy + ((outerRadius + 30) * sin);
  const ex = mx + ((cos >= 0 ? 1 : -1) * 22);
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  middlePieChartX = cx;
  middlePieChartY = cy;

  return (
    <g>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text x={ex + ((cos >= 0 ? 1 : -1) * 12)} y={ey} textAnchor={textAnchor} fill="#333">{`PV ${value}`}</text>
      <text x={ex + ((cos >= 0 ? 1 : -1) * 12)} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

renderActiveShape.propTypes = {
  cx: PropTypes.number,
  cy: PropTypes.number,
  midAngle: PropTypes.number,
  innerRadius: PropTypes.number,
  outerRadius: PropTypes.number,
  startAngle: PropTypes.number,
  endAngle: PropTypes.number,
  fill: PropTypes.string,
  payload: PropTypes.string,
  percent: PropTypes.number,
  value: PropTypes.number,
};

renderActiveShape.defaultProps = {
  cx: 0,
  cy: 0,
  midAngle: 0,
  innerRadius: 0,
  outerRadius: 0,
  startAngle: 0,
  endAngle: 0,
  fill: '#eee',
  payload: '',
  percent: 0,
  value: 0,
};

function CustomPieChart(props) {
  const { data, total } = props;
  const [activeIndex, setActiveIndex] = useState(0);

  const onPieEnter = (evt) => {
    const index = data.findIndex(p => p.name === evt.name);
    setActiveIndex(index);
  };

  return (
    <PieChart
      width={800}
      height={400}
      margin={{
        top: 5,
        right: 30,
        left: 80,
        bottom: 5
      }}
    >
      <text x={middlePieChartX} y={middlePieChartY} dy={8} textAnchor="middle">110 Total</text>
      <Pie
        dataKey="value"
        activeIndex={activeIndex}
        activeShape={renderActiveShape}
        data={data}
        cx={200}
        cy={200}
        innerRadius={80}
        outerRadius={150}
        fill={color.secondary}
        fillOpacity="0.8"
        onMouseEnter={(event) => onPieEnter(event)}
        label
      >
        {
          data.map((entry, index) => <Cell key={index.toString()} fill={colors[index % colors.length]} />)
        }
      </Pie>
      <Legend
        iconSize={10}
        width={120}
        height={140}
        layout="vertical"
        verticalAlign="middle"
        wrapperStyle={{
          top: 0,
          left: 550,
          lineHeight: '24px'
        }}
      />
    </PieChart>
  );
}

export default CustomPieChart;
