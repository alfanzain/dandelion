import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import Grid from '@material-ui/core/Grid';
import Filter from './Filter';
import Infographic from './Infographic';
import StatusOverview from './StatusOverview';
import ProjectOverview from './ProjectOverview';
import OperatorDocumentOverview from './OperatorDocumentOverview';
import EquipmentUtilizationOverview from './EquipmentUtilizationOverview';

function BasicTable() {
  const title = brand.name + ' - Dashboard';
  const description = brand.desc;
  const [dataFilter, setDataFilter] = useState({
    company: '',
    project: '',
    equipmentCode: '',
    createdDate: '',
    documentDate: '',
    nrp: '',
    documentNumber: '',
    approval1: '',
    approval2: '',
    approval3: '',
  });
  const handleChange = event => {
    setDataFilter({
      ...dataFilter,
      [event.target.name]: event.target.value
    });
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>

      <Grid container spacing={3}>
        <Grid
          container
          spacing={0}
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          <Grid item xs={8}>
            <Filter 
              dataFilter={dataFilter}
              handleChange={handleChange}
            />
          </Grid>
        </Grid>

        <Grid item md={12} sm={12} xs={12}>
          <Grid
            container
            spacing={3}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={12}>
              <Infographic />
            </Grid>
          </Grid>
        </Grid>

        <Grid item md={6} sm={12} xs={12}>
          <StatusOverview data={[
            {
              name: "Group A",
              value: 400,
            },
            {
              name: "Group B",
              value: 300,
            },
            {
              name: "Group C",
              value: 300,
            },
            {
              name: "Group D",
              value: 200,
            },
          ]} total={1200} />

          <OperatorDocumentOverview data={[
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
            [
              '8999999',
              '-',
              '-',
              '-',
              '-',
              '-',
            ],
          ]} />
        </Grid>

        <Grid item md={6} sm={12} xs={12}>
          <ProjectOverview data={{
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
              {
                label: 'My First dataset',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: [65, 59, 80, 81, 56, 55, 40]
              }
            ]
          }} />

          <EquipmentUtilizationOverview />
        </Grid>
      </Grid>
    </div>
  );
}

export default BasicTable;
