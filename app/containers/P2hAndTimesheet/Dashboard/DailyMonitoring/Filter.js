import React, { useState } from "react";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import MomentUtils from '@date-io/moment';

function Filter(props) {
  const { dataFilter, handleChange } = props;
  const [selectedDate, setSelectedDate] = useState(new Date());
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return (
    <div style={{ padding: 16, width: "100%" }}>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Filter
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={0}>
            <Grid 
              item 
              xs={12}
            >
              <MuiPickersUtilsProvider
                utils={MomentUtils}
              >
                <DatePicker
                  label="Report Date"
                  value={selectedDate}
                  onChange={handleDateChange}
                  animateYearScrolling={false}
                  style={{ margin: 8 }}
                />
              </MuiPickersUtilsProvider>
              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-company-code">Company</InputLabel>
                <Select
                  value={dataFilter.companyCode}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-company-code",
                    id: "filter-company-code",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Company Code 1</MenuItem>
                  <MenuItem value={20}>Company Code 2</MenuItem>
                  <MenuItem value={30}>Company Code 3</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-project">Project</InputLabel>
                <Select
                  value={dataFilter.project}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-project",
                    id: "filter-project",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl style={{ margin: 8, minWidth: 200 }}>
                <InputLabel htmlFor="filter-equipment-code">
                  Dashboard View
                </InputLabel>
                <Select
                  value={dataFilter.equipmentCode}
                  onChange={handleChange}
                  inputProps={{
                    name: "filter-equipment-code",
                    id: "filter-equipment-code",
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

export default Filter;
